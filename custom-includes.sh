# Put this block of code in your ~/.bashrc or ~/.vshrc file. You will need to
# change the variable CUSTOM_INCLUDE_PATH to point to where you are storing
# your custom shell scripts.
CUSTOM_INCLUDE_PATH=/path/to/shell/scripts
for i in "${CUSTOM_INCLUDE_PATH}"/*; do
    . "${i}"
done

# Put these (and other) custom scripts in a file that matches the above variable
# CUSTOM_INCLUDE_PATH. By doing this, every time a new shell session is created
# your custom scripts will be injected. Keeping these scripts out of your
# ~/.bashrc or ~/.zshrc allows you to back them up and make them portable. What
# is below is simply an example of some scripts that I've included in my own.
alias pkg-update='sudo su -c "apt clean && apt update -y && apt upgrade -y && apt autoremove"'
alias open='xdg-open'

function update-repos() {
    for DIR in "${1}"/*; do
        if [ -d "$DIR/.git" ]; then
            echo "Updating ${DIR}..."
            git -C "${DIR}" pull
        fi
    done
}

function netcheck() {
    local ADDR=$1
    if [ -z "$1" ]; then ADDR="8.8.8.8"; fi
    ping -c 3 $ADDR | while read pong; do echo "$(date): $pong"; done;
}