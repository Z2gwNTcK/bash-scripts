#!/bin/bash

# Automates the process of creating .desktop files for Linux systems where a Desktop application is not already created during
# the installation process.
function make-desktop-app() {
    local BASE="/usr/share/applications"
    local USAGE="usage: make-desktop-app [-n app name] [-c category list] [-e /path/to/exec] [-i /path/to/icon] [-t true|false]"
    local CATEGORY="Application"
    local TERMINAL="false"

    while getopts $"{:n:c:e:i:t:}" arg; do
        case ${arg} in
            n) local NAME=${OPTARG};;
            c) CATEGORY=${OPTARG};;
            e) local EXEC=${OPTARG};;
            i) local ICON=${OPTARG};;
            t) TERMINAL=${OPTARG};;
        esac
    done

    # If the user does not supply the name, path to the executable or the path to the icon file, the script will stop execution.
    if [ -z "${NAME+x}" ]; then echo $USAGE; echo "make-desktop-app: Please pass the name of the application."; return; fi
    if [ -z "${EXEC+x}" ]; then echo $USAGE; echo "make-desktop-app: Please pass the execution path to the application."; return; fi
    if [ -z "${ICON+x}" ]; then echo $USAGE; echo "make-desktop-app: Please pass the path to the application icon."; return; fi

    local DESKTOPFILE="[Desktop Entry]\nType=Application\nName=${NAME}\nCategories=${CATEGORY}\nExec=${EXEC}\nIcon=${ICON}\nTerminal=${TERMINAL}"
    local FILENAME=$(echo "$NAME" | awk '{print tolower($0)}' | sed -r 's/\s/-/g' | awk '{print $(0)".desktop"}')

    echo -e $DESKTOPFILE > $FILENAME
    sudo mv $FILENAME "${BASE}/${FILENAME}" && echo "Successfully created ${FILENAME} in ${BASE}."
}